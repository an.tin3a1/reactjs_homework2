import React, { useState } from 'react';
import { FaMicrophone, FaPlay, FaEllipsisH, FaRegHeart } from 'react-icons/fa';
import styles from './style.module.css';
import $ from 'jquery'
const Item = props => {
    const [isOpenDropDown, setIsOpenDropDown] = useState(false);
    const { song } = props
    function handleClick() {
        setIsOpenDropDown(!isOpenDropDown);
    }
    function ContextMenu(e) {

    }
    return (
        <li onContextMenu={() => { ContextMenu() }} key={song.id} className={styles.chartItem}>
            <div className={styles.detailSong}>
                <div className={styles.sort}>
                    <div className={styles.sortNumber}>
                        <span>{song.id}</span>
                    </div>
                    <div className={styles.sortReal}>
                        <span className={song.sortReal >= 0 ? styles.arrowUp : styles.arrowDown}></span>
                        <span className={styles.num}>{Math.abs(song.sortReal)}</span>
                    </div>
                </div>
                <div className={styles.thumb}>
                    <span className={styles.playIcon}><FaPlay color='white' /></span>
                    <img src={song.image} alt={song.title} />
                    <span className={styles.opac}></span>
                </div>
                <div className={styles.infoSong}>
                    <span className={styles.title}>{song.title}</span>
                    <span className={styles.artist}>{song.singer}</span>
                </div>
                <div className={styles.duration}>
                    <span>{song.duration}</span>
                </div>
                <div className={styles.extension}>
                    <ul className={styles.listButtons}>
                        <li><span><FaMicrophone /></span></li>
                        <li><span><FaRegHeart /></span></li>
                        <li><span onClick={() => { handleClick() }}><FaEllipsisH /></span></li>
                    </ul>
                </div>
            </div>
            {isOpenDropDown && <div className={styles.dropdownContent}>
                <a href="#">Thêm vào danh sách phát</a>
                <a href="#">Bình luận</a>
                <a href="#">Tải xuống</a>
                <a href="#">Sao chép link</a>
                <a href="#">Thông tin</a>
            </div>
            }
        </li>
    )
}
export default Item;