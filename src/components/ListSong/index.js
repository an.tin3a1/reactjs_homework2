import React from 'react';
import styles from './style.module.css';
import Item from '../../components/ListSong/Item'
const ListSong = props => {
    const { songs } = props
    return (
        <ul className={styles.listSong}>
            {
                songs && songs.map((song) => {
                    return <Item key={song.id} song={song} />
                })
            }
        </ul>
    )
}
export default ListSong;