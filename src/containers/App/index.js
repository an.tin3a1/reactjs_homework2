import React, { useState, useEffect } from 'react';
import './App.css';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import ListSong from '../../components/ListSong'
import data from "../../data/db.json";
function App() {
  const [items, setItems] = useState()
  useEffect(() => {
    data && setItems(data.items);
  }, [])

  return (
    <div className="App">
      <Container> <ListSong songs={items} /> </Container>
    </div>
  );
}

export default App;
